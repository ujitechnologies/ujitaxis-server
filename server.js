'use strict';

/**
 * Module dependencies.
 */
import start from './app/lib/app'

// Invoke the application thread
start()
