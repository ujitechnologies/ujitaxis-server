# Uji Taxis Application Logic(Server)

Uji Taxis is a service provision company, helping to provide solution to the ransporattion challenges in Nigeria. Uji taxis uses data optimized softwares to connect riders to drivers thereby reducing the time spent waiting to hail a taxi.

The main application login(server/backend) service is build on Node.JS. Node.Js is referred to as an asynchronous event driven JavaScript runtime, Node is designed to build scalable network applications. Node.Js can be used to handle many connections concurrently. Upon each connection the callback is fired, but if there is no work to be done, Node will sleep. To read more on Node.JS visit [nodejs.org](https://nodejs.org) which contains many information about Node.Js and guideline on its uses.

To extend the uses of Node.Js, the application includes Express.Js which is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications, express.js is used to privide routing, middleware and other services for the application.

## Setup/Installation Instructions

To install the application thie project assumes you have `node` and `npm` installed on your machine and you have a working `git` setup on your machine, if not you can quickly start that before moving for on the installation process, to confirm if you have any of the requiment mentioned above you can run the following command in your terminal or cmd. Also it is required to have MongoDB installed on your local machine as this will be using MongoDB as the database for the entire application. 

- `node -v` to confirm if you have node.js installed on your machine (minimum requirment of 8.11 or later)

- `npm -v` to confirm if you have npm (minimum requiremnt of 5.6 or later)

- `git -v` to confirm if you have git installed on your machine

- `mongob -v` to confirm if you have mongo db installed on your system

 If paraadventure you have none of the above mentioned requirment you can download the packages from the vendor website to install the software. If you have confirmed that you have every requirement for the application then you can proceed with installation steps.

#### Installation Guide



1. Clone the repo to your local machine using `git clone https://gitlab.com/ujitechnologies/ujitaxis-server.git`

2. Install the application dependencies from npm by running `npm install` or `yarn` in your terminal/command prompt in the project folder

3. After installation of the dependencies you are ready with every setup required and you can start the application.

4. Before you proceed, we need to set the environment variable for the application, to create the `.env` file run the `npm run copy:env` . This will copy the `.env.example` to `.env ` from the environment file you can take control of how the app works on your local machine.

5. Edit the `.env` file to configure how the application works on your local machine, some of the envirinment variables are not required and application can work with or without them. See below each configuration entails and how to setup th environment variables.

   1. `NODE_PATH`: This is the base directory for the application's source usually set to `.` , if you have installed the application in another directory and want to start the application from that direction then set the base path to the directory.

   2. `APPLICATION_ENVIRONMENT`: String, This is to set  main environment for the application to run usually `development`, `testing`, `production` , if you are running the application on your local machine you would need to it to `development` 

   3. `APP_NAME`: String, This is to set the application name usually `uji-server`

   4. `APP_PORT`: Integer, Set the port at which you want to start the application from, default is `8000`

   5. `APP_HOST`: String, Set the host at which the application should run, deafult `localhost`  

   6. `LOG_PATH`: String, Set the file or folder at which the app logger should log its file, default: `logs/uji-api.log`

   7. `LOG_LEVEL`: String, Log level you would prefer for winston which is the application logger middlware for express. Default: info

   8. `LOG_ENABLE_CONSOLE`: Boolean, set whether or not to enable winston logger on the console, Default: false

   9. `DB_PORT`: Integer, Set the port for the MongoDB: Default: `27017` (This requires the installation of Mongo Db on your local machine).

   10. `DB_HOST`: String, if you are working on a locally installed MongoDb this is usually `localhost` if you prefer to use cloud based.

   11. `DB_NAME`: String, The database name.

   12. `MONGOHQ_URL`: String, The mongo db URL/URI, Default: `mongo://`

   13. `MONGODB_DEBUG`: Boolean, Set whether you want to debug MongoDD or not

6. After completing the Environment setup, you are set and can start the application

7. To start the application, with your terminal opened in the project folder you can run `npm run serve` or `yarn serve`

8. The application will start using nodemon which means you don't need to run it and will auto restart anytime you make changes

9. To test the application run `npm run test` or `yarn test` and this would start the application test which utilizes jest for test the application

10. To start static type checking for the application utilizing `flow` run `npm run flow:check` 



That's all for the application





## Usage Instruction

This application requires that you have knowledge of Node.Js and Express Js before you can use the application, the application has been configured and connected to work fully as an API based application. 

The application has been divided into several folder which are `configs, controllers, lib, middlewares, models, modules, policies, routes, services, validations, and view` Each folder has its specific usage and associated files. See below the explanation of what each folder contains

- Configs: This contains the configuration file set up for the application, the confiuration file which will be used to distribute modules/librairies services.

- Lib: The lib folder contains the main application startup libraries, in this folder includes libraries which makes the main application work flow. Here we also have the express configurations and database configuration

- Middlewares: This contains folders/files for integrating middlewares for the extending application (express) and get registered as part of the express service. Here you set all the configuration for each middleware before fully connecting them to express

- Controllers: This contains the controller for each routes and API services.

- Models: This contains the blueprint of the databases

- Policies: This contains the rules applied to each roles and routes 

- Services: This contains aditional services need for the application

- Validations: This contains the rules to be applied to each models and services

- Modules: This is extracting of core services

- View: This contians the view for emails templates used by `express-hbs` 

- Routes: This contains the API endpoints for the application

> Please read the README file for each folder for better understanding of how to use the folder. In there you would understad the folder structure and file management struture



## Contribution Instruction

1. This project is strickly javascript based application and to extend the javascript we have integrated flow to allow for static type check and also babel to compile the javasript from ES6/7 to ES5 so we strickly support writing of each code in ES6/7 for easy understanding. 

2. This project has been confiured to use some certain rules of JavaScript and has integrated `eslint` to confirm and makse sure each code written is fully functional. To enable ESLint in your editor please guide on how activate eslint on your favorite editor.

3. This project strickly support to document and give clear comment for each file/LOC to allow for easy communication with other developers.






