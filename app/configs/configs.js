/**
 * Module dependencies.
 */
import _ from 'lodash'
import chalk from 'chalk'
import glob from 'glob'
import path from 'path'

/**
 * Get files by glob patterns
 */
const getGlobbedPaths = function (globPatterns, excludes) {
  // URL paths regex
  // eslint-disable-next-line
  const urlRegex = new RegExp('^(?:[a-z]+:)?\/\/', 'i')

  // The output array
  let output = []

  // If glob pattern is array then we use each pattern in a recursive way, otherwise we use glob
  if (_.isArray(globPatterns)) {
    globPatterns.forEach((globPattern) => {
      output = _.union(output, getGlobbedPaths(globPattern, excludes))
    })
  } else if (_.isString(globPatterns)) {
    if (urlRegex.test(globPatterns)) {
      output.push(globPatterns)
    } else {
      let files = glob.sync(globPatterns)
      if (excludes) {
        files = files.map((file) => {
          if (_.isArray(excludes)) {
            for (var i in excludes) {
              if (excludes.hasOwnProperty(i)) {
                file = file.replace(excludes[i], '');
              }
            }
          } else {
            file = file.replace(excludes, '')
          }
          return file
        })
      }
      output = _.union(output, files)
    }
  }

  return output
}


/**
 * Validate NODE_ENV existence
 */
const validateEnvironmentVariable = function () {
  const environmentFiles = glob.sync('./app/configs/env.js')
  // set the default environment to development
  if (!environmentFiles.length) {
    process.env.NODE_ENV = 'development'
  }
}

// TODO: Validating SSL Secure mode

/** Validate config.domain is set
 */
const validateDomainIsSet = function (config) {
  if (!config.app.domain) {
    console.log(chalk.red('+ Important warning: config.app.domain is empty. It should be set to the fully qualified domain of the app.'));
  }
}

/**
 * Initialize global configuration files
 */
const initGlobalConfigFiles = function (config, assets) {
  // Define object type
  config.files = {
    server: {}
  }

  // Setting Globbed model files
  config.files.server.models = getGlobbedPaths(assets.server.models)

  // Setting Globbed controller files 
  config.files.server.controllers = getGlobbedPaths(assets.server.controllers)

  // Setting Globbed route files
  config.files.server.routes = getGlobbedPaths(assets.server.routes)

  // Setting Globbed config files
  config.files.server.configs = getGlobbedPaths(assets.server.config)

  // Setting Globbed policies files
  config.files.server.policies = getGlobbedPaths(assets.server.policies)

  // Setting Globbed middleware
  config.files.server.middleware = getGlobbedPaths(assets.server.middleware)

  // Setting Globbed validation
  config.files.server.validations = getGlobbedPaths(assets.server.validations)
}

/**
 * Initialize global configuration
 */
const initGlobalConfig = function () {
  // Validate NODE_ENV existence
  validateEnvironmentVariable()

  // Get the default assets
  const assets = require(path.join(process.cwd(), 'app/configs/assets'))
  
  require('dotenv').config();
  // Get the default config
  const config = require(path.join(process.cwd(), 'app/configs/env'))

  // read package.json for Uji.js project information
  const pkg = require(path.resolve('./package.json'))
  config.ujijs = pkg

  // Initialize global globbed folders
  initGlobalConfigFiles(config, assets)

  // Print a warning if config.domain is not set
  validateDomainIsSet(config)

  // Expose configuration utilities
  config.utils = { getGlobbedPaths }

  return config
}

/**
 * Set configuration object
 */
export default initGlobalConfig()
