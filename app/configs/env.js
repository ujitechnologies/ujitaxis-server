module.exports = {
  app: {
    name: process.env.APP_NAME || 'uji-taxis-server',
    host: process.env.APP_HOST || 'localhost',
    port: process.env.APP_PORT || 8000,
    get domain(){
      return `${this.host}:${this.port}` || 'localhost:8000'
    },
    environment: process.env.APPLICATION_ENV,
    logpath: process.env.LOG_PATH
  },
  mongo: {
    name: process.env.DB_NAME,
    address: (process.env.NODE_ENV=='production') ? process.env.DB_1_PORT_27017_TCP_ADDR : (`${process.env.DB_HOST}:${process.env.DB_PORT}/`),
    mongoUrl: process.env.MONGOHQ_URL || process.env.MONGODB_URI || 'mongodb://',
    get uri() {
      return this.mongoUrl + this.address + this.name
    },
    promise: global.Promise,
    debug: process.env.MONGODB_DEBUG || false,
    options: {
      // TODO: passin in db options here
    }
  },
  uploads: {
    profile: {
      image: {
        dest: process.env.PROFILE_IMG_DEST,
        limits: {
          fileSize: process.env.PROFILE_FILE_SIZE
          // TODO: pass in the filesize in number math format
        }
      }
    }
  },
  shared: {
    owasp: {
      allowPassphrases: true,
      maxLength: 128,
      minLength: 10,
      minPhraseLength: 20,
      minOptionalTestsToPass: 4
    }
  },
  mailer: {
    from: process.env.MAILER_FROM,
    apiKey: process.env.MAILGUN_API_KEY,
    domain: process.env.MAILGUN_DOMAIN,
    emailFrom: process.env.EMAIL_FROM,
    options: {
      service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
      auth: {
        user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
        pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
      }
    }
  },
  application_logging: {
    file: process.env.LOG_PATH,
    level: process.env.LOG_LEVEL || 'info',
    console: process.env.LOG_ENABLE_CONSOLE || true
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    expiringTime: process.env.JWT_EXPIRING_TIME || '60 * 60 * 24 * 7'
  }
}
