/* eslint comma-dangle:[0, "only-multiline"] */
module.exports = {
  server: {
    allJS: ['server.js', 'app/config/*.js', 'app/lib/*.js'],
    middlewares: 'app/middlewares/**/*.js',
    models: 'app/models/**/*.js',
    controllers: 'app/controllers/**/*.js',
    routes: 'app/routes/**/*.js',
    config: 'app/modules/**/*.js',
    services: 'app/services/**/*.js',
    policies: 'app/policies/**/*.js',
    validations: 'app/validations/**/*.js',
    views: 'app/views/**/*.html'
  }
}
