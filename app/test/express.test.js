import request from 'supertest'
import express from '../lib/express'
const app = express()
describe('Testing the app path', () => {
  test('It should response the GET method', (done) => {
    request(app).get('/app/testing').then((response) => {
      expect(response.statusCode).toBe(200)
      done()
    })
  })
})