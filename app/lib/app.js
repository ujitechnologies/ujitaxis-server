/**
 * Module dependencies.
 */

import config from '../configs/configs'
import connect, {loadModels} from './mongoose'
import express from './express'
import chalk from 'chalk'

const appversion = require('../../package.json').version

// Initialize Models
loadModels()

/**
 * Initialize express and mongo
 * @param {*} cb 
 */
export function init(cb) {
  // Connect to mongo db
  connect((db) => {
    // Initialize express
    const app = express(db)
    // Run the callback function passes
    if (cb) cb(app, db, config)
  })
}

/**
 * start the main application
 * @param {*} callback 
 */
export default function start(cb) {
  init(function (app, db, config) {

    // Start the app by listening on <port> at <host>
    app.listen(config.app.port, config.app.host, () => {
      // Create server URL
      const server = (process.env.NODE_ENV === 'secure' ? 'https://' : 'http://') + config.app.host + ':' + config.app.port
      const uri = config.mongo.mongoUrl + config.mongo.address + config.mongo.name
      // Logging initialization
      console.log('--')
      console.log(chalk.green(config.app.name));
      console.log()
      console.log(chalk.green('Environment:     ' + config.app.environment))
      console.log(chalk.green('Server:          ' + server))
      console.log(chalk.green('Database:        ' + uri))
      console.log(chalk.green('App version:     ' + appversion))
      console.log('--')

      // Run any callback set after intialization
      if (cb) cb(app, db, config)
    })
  })
}
