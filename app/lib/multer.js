/**
 * Multer configuration
 * @param {*} req 
 * @param {*} file 
 * @param {*} callback 
 */
export default function imageFileFilter(req, file, cb) {
  // Check the file type for the uploaded file
  if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/gif') {
    const err = new Error()
    // Change the error code
    err.code = 'UNSUPPORTED_MEDIA_TYPE'
    return cb(err, false)
  }
  cb(null, true)
}
