/**
 * Module dependencies.
 */
import config from '../configs/configs'
import chalk from 'chalk'
import path from 'path'
import mongoose from 'mongoose'

/**
 * Load all mongo models from the model folder
 * @param {*} cb 
 * @return {function} All models
 */
export function loadModels(cb) {
  // Globbing model files
  config.files.server.models.forEach((modelPath) => {
    require(path.resolve(modelPath))
  })
  
  // Run any callback function attached to the function
  if (cb) callback()
}

/**
 * Connect to the database
 * @param {*} cb 
 */
export default function connect(cb) {
  mongoose.Promise = config.mongo.promise;
  const db = mongoose.connect(config.mongo.uri, config.mongo.options, (err) => {
    // Log Error
    if (err) return console.error(chalk.red('Could not connect to MongoDB!'), err)
    // Enabling mongoose debug mode if required
    mongoose.set('debug', config.mongo.debug)

    // Call callback FN
    if (cb) cb(db)
  })
}

/**
 * Disconnect from the databse
 * @param {*} cb 
 */
export function disconnect(cb) {
  mongoose.disconnect(function (err) {
    console.info(chalk.yellow('Disconnected from MongoDB.'))
    cb(err)
  })
}
