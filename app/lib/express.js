'use strict';

/**
 * Module dependencies.
 */
import express from 'express'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import methodOverride from 'method-override'
import cookieParser from 'cookie-parser'
import path from 'path'
import cors from 'cors'
import hbs from 'express-hbs'
import config from '../configs/configs'
import logger from './logger'

/**
 * Initialize local variables
 */
const initLocalVariables = (app) => {
  // Setting application local variables
  app.locals.title = config.app.name

  if (config.secure && config.secure.ssl === true) {
    app.locals.secure = config.secure.ssl
  }

  app.locals.env = config.app.environment
  app.locals.domain = config.app.domain

  // Passing the request url to environment locals
  app.use((req, res, next) => {
    res.locals.host = req.protocol + '://' + req.hostname
    res.locals.url = req.protocol + '://' + req.headers.host + req.originalUrl
    next()
  })
}

/**
 * Initialize application middleware
 */
const initMiddleware = (app) => {

  // Enable logger (morgan) if enabled in the configuration file

  app.use(morgan('combined', logger.create(config)))

  // Request body parsing middleware should be above methodOverride
  app.use(bodyParser.urlencoded({
    extended: true
  }))

  app.use(cors())
  app.use(bodyParser.json())
  app.use(methodOverride())
  app.use(express.static(path.resolve('public')))
  // Add the cookie parser middleware
  app.use(cookieParser())
}

/**
 * Configure view engine
 */
const initViewEngine = (app) => {
  app.engine('view.html', hbs.express4({
    extname: '.view.html'
  }))
  app.set('view engine', 'view.html')
  app.set('views', path.resolve('./'))
}

/**
 * Invoke modules server configuration
 */
const initModulesConfiguration = (app, db) => {
  config.files.server.configs.forEach((configPath) => {
    require(path.resolve(configPath))(app, db)
  })
}

/**
 * Configure the modules ACL policies
 */
const initModulesServerPolicies = (app) => {
  // Globbing policy files
  config.files.server.policies.forEach((policyPath) => {
    require(path.resolve(policyPath)).invokeRolesPolicies()
  })
}

/**
 * Configure the modules server routes
 */
const initModulesServerRoutes = (app) => {
  // Globbing routing files
  config.files.server.routes.forEach((routePath) => {
    require(path.resolve(routePath))(app)
  })
}

/**
 * Configure error handling
 */
const initErrorRoutes = (app) => {
  app.use((err, req, res, next) => {
    // If the error object doesn't exists 
    if (!err) return next()

    // Log it
    console.error(err.stack)
  })
}


/**
 * Initialize the Express application
 */
export default function init(db) {
  // Initialize express app
  var app = express()
  // Initialize local variables
  initLocalVariables(app)

  // Initialize Express middleware
  initMiddleware(app)

  // Initialize Express view engine
  initViewEngine(app)

  // Initialize Modules configuration
  initModulesConfiguration(app)

  // Initialize modules server authorization policies
  initModulesServerPolicies(app)

  // Initialize modules server routes
  initModulesServerRoutes(app)

  // Initialize error routes
  initErrorRoutes(app)

  return app
}
