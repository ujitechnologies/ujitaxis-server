/**
 * Module dependencies
 */
import winston, {createLogger, format, transports} from 'winston'
const {combine, timestamp, label, prettyPrint} = format


const createTransports = function (config) {
  const customTransports = []

  // setup the file transport
  if (config.file) {

    // setup the log transport
    customTransports.push(
      new transports.File({
        filename: config.file,
        level: config.level
      })
    )
  }

  // if config.console is set to true, a console logger will be included.
  if (config.console) {
    customTransports.push(
      new transports.Console({
        level: config.level
      })
    )
  }

  return customTransports;
}

const logger = (config) => {
  winston.createLogger({
    transports: createTransports(config),
        format: combine(
          label({label: 'UJI API'}),
          timestamp(),
          prettyPrint()
        )
  })
}

export default {
  create: (config) => {
    return logger(config)
  }
}
