import {welcome} from '../../controllers/testing/testing'

// TODO: Importing es6 module and passing arguments
module.exports = (app) => {
  // Setting up the users authentication api
  app.route('/api/testing')
    .get(welcome);
}

// export default {
//   routes(app) {
//     app.route('/api/testing')
//     .get(welcome);
//   }
// }